﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ostunimekiri.DataModels.Models;

namespace Ostunimekiri.DAL
{
    public class Program
    {
        public static void Main(string[] args)
        {
            using (var db = new ShoppingContext())
            {
                Console.WriteLine(db.Products.Add(new Product()
                {
                    IsDeleted = false,
                    Name = "tomat"
                }));
            }
        }
    }
}
