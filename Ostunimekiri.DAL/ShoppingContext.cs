﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using Ostunimekiri.DataModels.Models;


namespace Ostunimekiri.DAL
{
    public class ShoppingContext : IdentityDbContext<IdentityUser>
    {

        public ShoppingContext()
            : base("ShoppingContext")
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<ShoppingContext>());
        }

        public DbSet<ShoppingList> ShoppingLists { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Shop> Shops { get; set; }
        public DbSet<Request> Requests { get; set; }
        public DbSet<ShoppingListProduct> ShoppingListProducts { get; set; }
        public DbSet<ShoppingListUser> ShoppingListUsers { get; set; }
    }
}