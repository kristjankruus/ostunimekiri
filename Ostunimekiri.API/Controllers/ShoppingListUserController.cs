﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Ostunimekiri.DataModels.Models;
using Ostunimekiri.Repos;
using Ostunimekiri.Repos.Contracts;
using Ostunimekiri.TransportLogic;
using Ostunimekiri.TransportModels;

namespace Ostunimekiri.API.Controllers
{
    // [Authorize]
    public class ShoppingListUserController : ApiController
    {
        private readonly IShoppingListUserRepository _repo = new ShoppingListUserRepository();
        private ModelFactory _factory = new ModelFactory();
        private ShoppingListUserLogic _logic = new ShoppingListUserLogic();
        // GET: api/ShoppingListUser
        public ShoppingListUser Get(int id)
        {
            ShoppingListUser user = _repo.GetById(id);
            return user;
        }
        [Route("api/ShoppingListUser/GetUser")]
        public List<ShoppingListUserDTO> GetUser(int id)
        {
            List<ShoppingListUserDTO> users = _logic.GetUsers(id);
            return users;
        }

        public HttpResponseMessage PostShoppingListUser(ShoppingListUser shoppingListUser)
        {
            _repo.AddShoppingListUser(shoppingListUser);
            return new HttpResponseMessage(HttpStatusCode.Created);
        }

        public HttpResponseMessage DeleteShoppingListUser(int id)
        {
            _repo.MarkShoppingListUserDeleted(id);
            return new HttpResponseMessage(HttpStatusCode.OK);
        }
    }
}