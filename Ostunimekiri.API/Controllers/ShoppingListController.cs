﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Ajax.Utilities;
using Ostunimekiri.DataModels.Models;
using Ostunimekiri.Repos;
using Ostunimekiri.Repos.Contracts;
using Ostunimekiri.TransportLogic;
using Ostunimekiri.TransportModels;

namespace Ostunimekiri.API.Controllers
{
    public class ShoppingListController : ApiController
    {
        private readonly IShoppingListRepository _repo = new ShoppingListRepository();
        private ModelFactory _factory = new ModelFactory();
        private ShoppingListLogic _logic = new ShoppingListLogic(new ShoppingListRepository());

        // GET: api/ShoppingList?userName={userName}
        // [Authorize]
        public List<ShoppingListDTO> GetShoppingList(string userName)
        {
            return _logic.GetShoppingList(userName);
        }
        //GET api/ShoppingList?shoppingListId={shoppingListId}
        public ShoppingListDTOBig GetShoppingListById(int shoppingListId)
        {
            return _logic.GetShoppingListDtoBig(shoppingListId);
        }

        public List<ShoppingList> GetShoppingListBetweenDates(string userName, DateTime startTime, DateTime endTime)
        {
            return _repo.GetBetweenDates(userName, startTime, endTime);
        }
        // POST: api/ShoppingList
        public HttpResponseMessage PostShoppingList(ShoppingList shoppingList)
        {
            _repo.AddNewShoppingList(shoppingList);
            return new HttpResponseMessage(HttpStatusCode.Created);
        }

        //PUT: api/ShoppingList 
        public HttpResponseMessage PutShoppingList(ShoppingList shoppingList)
        {
            _repo.UpdateShoppingList(shoppingList);
            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        // DELETE: api/ShoppingList/5
        public HttpResponseMessage DeleteShoppingList(int shoppingListId)
        {
            _repo.MarkShoppingListInActive(shoppingListId);
            return new HttpResponseMessage(HttpStatusCode.OK);
        }
    }
}