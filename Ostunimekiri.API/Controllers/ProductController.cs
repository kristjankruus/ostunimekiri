﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Ostunimekiri.Repos;
using Ostunimekiri.Repos.Contracts;

namespace Ostunimekiri.API.Controllers
{
    public class ProductController : ApiController
    {
        IProductRepository _repo = new ProductRepository();
        // GET: api/Product/5
        public List<string> Get(string name)
        {
            return _repo.GetAll().Where(x => x.Name.Contains(name) && x.IsDeleted == false).Select(x => x.Name).ToList();
        }
    }
}
