﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Ostunimekiri.DataModels.Models;
using Ostunimekiri.Repos;
using Ostunimekiri.Repos.Contracts;
using Ostunimekiri.TransportLogic;
using Ostunimekiri.TransportModels;

namespace Ostunimekiri.API.Controllers
{
    //[Authorize]
    public class ShoppingListProductController : ApiController
    {
        private readonly IShoppingListProductRepository _repo = new ShoppingListProductRepository();
        private ModelFactory _factory = new ModelFactory();
        private ShoppingListProductLogic _logic = new ShoppingListProductLogic(new ShoppingListProductRepository());

        public List<ShoppingListProductDTO> GetShoppingListProducts(int shoppingListId)
        {
            return _logic.GetShoppingListProductDto(shoppingListId);
        }

        // POST: api/ShoppingListProduct
        public HttpResponseMessage Post(List<ShoppingListProduct> shoppingListProduct)
        {
            _repo.PostListShoppingListProduct(shoppingListProduct);
            return new HttpResponseMessage(HttpStatusCode.Created);
        }

        // PUT: api/ShoppingListProduct/5
        public HttpResponseMessage Put(List<ShoppingListProduct> products)
        {
            _repo.PutListOfShoppingListProduct(products);
            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        // DELETE: api/ShoppingListProduct/5
        public HttpResponseMessage Delete(int id)
        {
            _repo.RemoveProductFromExistingShoppingList(id);
            return new HttpResponseMessage(HttpStatusCode.OK);
        }
    }
}
