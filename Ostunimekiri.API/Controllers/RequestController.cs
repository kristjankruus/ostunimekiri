﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Ostunimekiri.DataModels.Models;
using Ostunimekiri.Repos;
using Ostunimekiri.Repos.Contracts;
using Ostunimekiri.TransportLogic;
using Ostunimekiri.TransportModels;

namespace Ostunimekiri.API.Controllers
{
    // [Authorize]
    public class RequestController : ApiController
    {
        private readonly IRequestRepository _repo = new RequestRepository();
        private ModelFactory _factory = new ModelFactory();
        private RequestLogic _logic = new RequestLogic();

        // GET: api/Request
        public int GetRequestCount(string userId)
        {
            return _repo.GetRequestById(userId).Count;
        }

        // GET: api/Request
        [Route("api/Request/GetRequests")]
        public List<RequestDTO> GetRequests(string userId)
        {
            return _logic.GetRequests(userId);
        }

        public List<RequestDTO> GetListRequests(int listId)
        {
            return _logic.GetListRequests(listId);
        }

        // POST: api/Request
        public HttpResponseMessage Post(List<Request> requests)
        {
            foreach (var request in requests)
            {
                _repo.AddRequests(request);
            }
            return new HttpResponseMessage(HttpStatusCode.Created);
        }

        // PUT: api/Request
        public HttpResponseMessage Put(Request request)
        {
            _repo.AcceptRequest(request);
            return new HttpResponseMessage(HttpStatusCode.Created);
        }

        // DELETE: api/Request/5
        public HttpResponseMessage Delete(int id)
        {
            _repo.CancelRequest(id);
            return new HttpResponseMessage(HttpStatusCode.OK);
        }
    }
}
