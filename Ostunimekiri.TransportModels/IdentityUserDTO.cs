﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ostunimekiri.TransportModels
{
    public class IdentityUserDTO
    {
        public string Id { get; set; }
        public string UserName { get; set; }
    }
}
