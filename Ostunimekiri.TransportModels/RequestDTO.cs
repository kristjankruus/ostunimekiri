﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using Ostunimekiri.DataModels.Models;

namespace Ostunimekiri.TransportModels
{
    public class RequestDTO
    {
        public int RequestId { get; set; }
        public int ShoppingListId { get; set; }
        public string IdentityUserId { get; set; }
        public IdentityUserDTO IdentityUser { get; set; }
        public ShoppingListDTO ShoppingList { get; set; }
        public DateTime RequestTime { get; set; }
        public string InvitorName { get; set; }
        public bool IsAccepted { get; set; }
        public bool IsDeleted { get; set; }
    }
}
