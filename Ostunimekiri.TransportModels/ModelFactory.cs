﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using Ostunimekiri.DataModels.Models;

namespace Ostunimekiri.TransportModels
{
    public class ModelFactory
    {
        public ProductDTO ProductModel(Product product)
        {
            return new ProductDTO()
            {
                ProductId = product.ProductId,
                Name = product.Name
            };
        }

        public ShoppingListDTO ShoppingList(ShoppingList shoppingList)
        {
            return new ShoppingListDTO()
            {
                ShoppingListId = shoppingList.ShoppingListId,
                Name = shoppingList.Name,
                Created = shoppingList.Created,
                Users = ShoppingListUserDto(shoppingList.ShoppingListUsers)
            };
        }

        public List<ShoppingListUserDTO> ShoppingListUserDto(List<ShoppingListUser> shoppingListUser)
        {
            List<ShoppingListUserDTO> shoppingListUserDtos = new List<ShoppingListUserDTO>();
            foreach (var user in shoppingListUser)
            {
                shoppingListUserDtos.Add(new ShoppingListUserDTO()
                {
                    ShoppingListUserId = user.ShoppingListUserId,
                    IdentityUserId = user.IdentityUserId,
                    IdentityUserName = user.IdentityUser.UserName,
                    ShoppingListId = user.ShoppingListId,
                    JoinTime = user.JoinTime
                });
            }
            return shoppingListUserDtos;
        }

        public ShoppingListDTOBig ShoppingListBig(ShoppingList shoppingList)
        {
            return new ShoppingListDTOBig()
            {
                Created = shoppingList.Created,
                IsDeleted = shoppingList.IsDeleted,
                Name = shoppingList.Name,
                Products = shoppingList.Products,
                ShoppingListId = shoppingList.ShoppingListId,
                Users = shoppingList.ShoppingListUsers
            };
        }

        public ShopDTO ShopModel(Shop shop)
        {
            if (shop != null)
            {
                return new ShopDTO()
                {
                    ShopId = shop.ShopId,
                    Name = shop.Name
                };
            }
            return new ShopDTO();
        }

        public ShoppingListProductDTO ShoppingListProduct(ShoppingListProduct shoppingListProduct)
        {
            return new ShoppingListProductDTO()
            {
                ShoppingListId = shoppingListProduct.ShoppingListId,
                ShoppingListProductId = shoppingListProduct.ShoppingListProductId,
                IsTaken = shoppingListProduct.IsTaken,
                Quantity = shoppingListProduct.Quantity,
                Price = shoppingListProduct.Price,
                Shop = ShopModel(shoppingListProduct.Shop),
                Product = ProductModel(shoppingListProduct.Product)
            };
        }

        public IdentityUserDTO FactoryIdentityUser(IdentityUser identityUser)
        {
            return new IdentityUserDTO()
            {
                Id = identityUser.Id,
                UserName = identityUser.UserName
            };
        }

        public RequestDTO Request(Request request)
        {
            return new RequestDTO()
            {
                RequestId = request.RequestId,
                IsAccepted = request.IsAccepted,
                IsDeleted = request.IsDeleted,
                RequestTime = request.RequestTime,
                InvitorName = request.InvitorName,
                ShoppingListId = request.ShoppingListId,
                ShoppingList = ShoppingList(request.ShoppingList),
                IdentityUserId = request.IdentityUserId,
                IdentityUser = FactoryIdentityUser(request.IdentityUser)
            };
        }
    }
}
