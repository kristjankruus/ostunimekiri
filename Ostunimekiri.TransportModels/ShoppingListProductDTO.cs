﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ostunimekiri.DataModels.Models;

namespace Ostunimekiri.TransportModels
{
    public class ShoppingListProductDTO
    {
        public int ShoppingListProductId { get; set; }
        public int ShoppingListId { get; set; }
        public double Quantity { get; set; }
        public bool IsTaken { get; set; }
        public double Price { get; set; }
        public ProductDTO Product { get; set; }
        public ShopDTO Shop { get; set; }
    }
}
