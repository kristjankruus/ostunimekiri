﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using Ostunimekiri.DataModels.Models;

namespace Ostunimekiri.TransportModels
{
    public class ShoppingListDTOBig
    {
        public int ShoppingListId { get; set; }
        public string Name { get; set; }
        public List<ShoppingListProduct> Products { get; set; }
        public List<ShoppingListUser> Users { get; set; }
        public List<Request> Requests { get; set; }
        public DateTime Created { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class ShoppingListDTO
    {
        public int ShoppingListId { get; set; }
        public DateTime Created { get; set; }
        public string Name { get; set; }
        public List<ShoppingListUserDTO> Users { get; set; }
    }
}
