﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Ostunimekiri.DataModels.Models
{
    public class Product
    {
        public int ProductId { get; set; }
        [MaxLength(32)]
        public string Name { get; set; }
        public virtual List<ShoppingListProduct> ShoppingListProducts { get; set; }
        public bool IsDeleted { get; set; }
    }
}
