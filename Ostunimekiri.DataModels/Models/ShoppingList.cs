﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;


namespace Ostunimekiri.DataModels.Models
{
    public class ShoppingList
    {
        public int ShoppingListId { get; set; }
        [MaxLength(32)]
        public string Name { get; set; }
        public DateTime Created { get; set; }
        public bool IsActive { get; set; }
        public virtual List<ShoppingListProduct> Products { get; set; }
        public virtual List<Request> Requests { get; set; }
        public virtual List<ShoppingListUser> ShoppingListUsers { get; set; }
        public bool IsDeleted { get; set; }
    }
}
