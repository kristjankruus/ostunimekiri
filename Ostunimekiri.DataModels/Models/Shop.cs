﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ostunimekiri.DataModels.Models
{
    public class Shop
    {
        public int ShopId { get; set; }
        public virtual List<ShoppingListProduct> ShoppingListProducts { get; set; }
        [MaxLength(32)]
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
    }
}
