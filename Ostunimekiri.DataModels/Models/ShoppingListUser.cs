﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Ostunimekiri.DataModels.Models
{
    public class ShoppingListUser
    {
        public int ShoppingListUserId { get; set; }
        [MaxLength(50)]
        public string IdentityUserId { get; set; }
        public virtual IdentityUser IdentityUser { get; set; }
        public int ShoppingListId { get; set; }
        public virtual ShoppingList ShoppingList { get; set; }
        public DateTime JoinTime { get; set; }
        public bool IsDeleted { get; set; }
    }
}
