﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Ostunimekiri.DataModels.Models
{
    public class ShoppingListProduct
    {
        public int ShoppingListProductId { get; set; }
        public int ShoppingListId { get; set; }
        public virtual ShoppingList ShoppingList { get; set; }
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
        public int? ShopId { get; set; }
        public virtual Shop Shop { get; set; }
        public double Quantity { get; set; }
        public double Price { get; set; }
        public bool IsTaken { get; set; }
        public bool IsDeleted { get; set; }
    }
}
