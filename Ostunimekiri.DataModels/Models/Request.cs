﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Ostunimekiri.DataModels.Models
{
    public class Request
    {
        public int RequestId { get; set; }
        public int ShoppingListId { get; set; }
        public virtual ShoppingList ShoppingList { get; set; }
        [MaxLength(50)]
        public string IdentityUserId { get; set; }
        public virtual IdentityUser IdentityUser { get; set; }
        public string InvitorName { get; set; }
        public DateTime RequestTime { get; set; }
        public bool IsAccepted { get; set; }
        public bool IsDeleted { get; set; }
    }
}
