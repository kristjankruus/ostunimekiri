﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ostunimekiri.DataModels.Models;
using Ostunimekiri.Repos.Contracts;

namespace Ostunimekiri.Repos
{
    public class ProductRepository : EFRepository<Product>, IProductRepository
    {

        public Product GetByName(string productName)
        {
            return base.GetAll().ToList().FirstOrDefault(x => x.Name == productName);
        }

        public Product GetProduct(int productID)
        {
            return base.GetAll().Single(x => x.IsDeleted == false && x.ProductId == productID);
        }

        public void AddProduct(Product product)
        {
            List<Product> productsList = base.GetAll()
                .Where(x => x.IsDeleted == false)
                .Where(x => x.Name == product.Name)
                .ToList();

            if (productsList.Count > 1)
            {
                Product exProduct = productsList.FirstOrDefault();
                productsList.RemoveAll(r => r != exProduct);
            }
        }
    }
}
