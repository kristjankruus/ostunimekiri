﻿using Ostunimekiri.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Ostunimekiri.Repos.Contracts;

namespace Ostunimekiri.Repos
{
    public class EFRepository<T> : IRepository<T> where T : class
    {
        public EFRepository()
        {
            DbContext = new ShoppingContext();
            DbSet = DbContext.Set<T>();
        }

        protected DbContext DbContext { get; set; }

        protected DbSet<T> DbSet { get; set; }

        public virtual IQueryable<T> GetAll()
        {
            return DbSet;
        }

        public virtual IQueryable<T> GetAllIncluding(params Expression<Func<T, object>>[] includeProperties)
        {
            return includeProperties.
                Aggregate<Expression<Func<T, object>>, IQueryable<T>>(DbSet,
                  (current, includeProperty) => current.Include(includeProperty));
        }

        public T GetById(int id)
        {
            return DbSet.Find(id);
        }

        public void Add(T entity)
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State != EntityState.Detached)
            {
                dbEntityEntry.State = EntityState.Added;
            }
            else
            {
                DbSet.Add(entity);
            }
        }

        //public void AddOrUpdate(T entity)
        //{
        //   DbEntityEntry dbEntityEntry = DbContext.Entry(entity);

        //    if(dbEntityEntry.State
        //}

        public virtual void Update(T entity)
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State == EntityState.Detached)
            {
                DbSet.Attach(entity);
            }
            dbEntityEntry.State = EntityState.Modified;
        }

        public void Delete(T entity)
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State != EntityState.Deleted)
            {
                dbEntityEntry.State = EntityState.Deleted;
            }
            else
            {
                DbSet.Attach(entity);
                DbSet.Remove(entity);
            }
        }

        public void Delete(int id)
        {
            var entity = GetById(id);
            if (entity == null) return;
            Delete(entity);
        }

        public void Dispose()
        {
            if (DbContext == null) return;
            DbContext.Dispose();
            DbContext = null;
        }

        public void SaveChanges()
        {
            DbContext.SaveChanges();
        }

        //public void AddAndSave(T entity) {
        //    this.Add(entity);
        //    this.SaveChanges();
        //}
    }
}
