﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ostunimekiri.DataModels.Models;

namespace Ostunimekiri.Repos.Contracts
{
    public interface IRequestRepository : IRepository<Request>
    {
        /// <summary>
        /// Tagastab listi kutsetest kasutaja ID järgi 
        /// </summary>
        /// <param name="id">IdentityUserID</param>
        /// <returns>List kutsetest</returns>
        List<Request> GetRequestById(string id);

        /// <summary>
        /// Tagastab Listi kutsetest ostunimekirja ID järgi
        /// </summary>
        /// <param name="id">Vastava ostunimekirja ID</param>
        /// <returns>List kutsestest</returns>
        List<Request> GetRequestByListId(int id);

        /// <summary>
        /// Lisab uue kutse liituda ostunimekirjaga
        /// </summary>
        /// <param name="request">Uus kutse</param>
        void AddRequests(Request request);

        /// <summary>
        /// Uus kutse võetakse vastu
        /// </summary>
        /// <param name="request">Olemasolev kutse</param>
        void AcceptRequest(Request request);

        /// <summary>
        /// Tühistatakse vastav kutse
        /// </summary>
        /// <param name="requestId">Kutse iD</param>
        void CancelRequest(int requestId);
    }
}
