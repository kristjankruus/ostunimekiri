﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ostunimekiri.DataModels.Models;

namespace Ostunimekiri.Repos.Contracts
{
    public interface IShopRepository : IRepository<Shop>
    {
        /// <summary>
        /// Tagastatakse pood ID järgi
        /// </summary>
        /// <param name="shopId">Poe ID</param>
        /// <returns>Vastava ID-ga pood</returns>
        Shop GetShopById(int shopId);

        /// <summary>
        /// Lisatakse uus pood
        /// </summary>
        /// <param name="shop">Uus pood</param>
        void AddShop(Shop shop);

        /// <summary>
        /// Muudetakse olemasolevat poodi
        /// </summary>
        /// <param name="shop">Muudetud pood</param>
        void PutShop(Shop shop);

        /// <summary>
        /// Kustutatakse pood
        /// </summary>
        /// <param name="shopId">Poe ID</param>
        void MarkShopDeleted(int shopId);
    }
}
