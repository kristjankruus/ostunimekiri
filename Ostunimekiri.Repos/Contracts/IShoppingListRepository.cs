﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using Ostunimekiri.DataModels.Models;

namespace Ostunimekiri.Repos.Contracts
{
    public interface IShoppingListRepository : IRepository<ShoppingList>
    {
        /// <summary>
        /// Tagastab listi kasutaja poenimekirjadest
        /// </summary>
        /// <param name="userId">Kasutaja id</param>
        /// <returns>kasutaja poenimekirjad</returns>
        List<ShoppingList> GetUserShoppingLists(string userName);

        /// <summary>
        /// Tagastab ostunimekirja vastava ID järgi
        /// </summary>
        /// <param name="shoppingListId">Ostunimekirja ID</param>
        /// <returns>Nimekiri vastava ID järgi</returns>
        ShoppingList GetShoppingList(int shoppingListId);


        /// <summary>
        /// Tagastab listi kasutaja poenimekirjadest
        /// </summary>
        /// <param name="userId">Kasutaja id</param>
        /// <returns>kasutaja poenimekirjad</returns>
        List<ShoppingList> GetBetweenDates(string userName, DateTime startTime, DateTime endTime);

        /// <summary>
        /// Lisatakse uus poenimekiri
        /// </summary>
        /// <param name="list">Omanik</param>
        /// <returns>Loodud nimekiri</returns>
        void AddNewShoppingList(ShoppingList shoppingList);

        /// <summary>
        /// Muudab olemasolevat poenimekirja
        /// </summary>
        /// <param name="shoppingList">Uuendatud poenimekiri</param>
        void UpdateShoppingList(ShoppingList shoppingList);

        /// <summary>
        /// Toote eemaldamine nimekirjast
        /// </summary>
        /// <param name="product">Eemaldatav toode</param>
        /// <param name="shoppingList">Vastav ostunimekiri</param>
        void MarkShoppingListInActive(int shoppingListId);
    }
}
