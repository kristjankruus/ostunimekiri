﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ostunimekiri.DataModels.Models;

namespace Ostunimekiri.Repos.Contracts
{
    public interface IProductRepository : IRepository<Product>
    {
        /// <summary>
        /// Tagastatakse vastav toode
        /// </summary>
        /// <param name="productID">Toote iD</param>
        /// <returns>Vastava ID-ga toode</returns>
        Product GetProduct(int productID);

        /// <summary>
        /// Lisame toote baasi
        /// </summary>
        /// <param name="product">lisatav toode</param>
        void AddProduct(Product product);
    }
}
