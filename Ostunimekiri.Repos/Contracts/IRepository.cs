﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Ostunimekiri.Repos.Contracts
{
    public interface IRepository<T> : IDisposable where T : class
    {
        IQueryable<T> GetAll();
        // get all records with filter
        IQueryable<T> GetAllIncluding(params Expression<Func<T, object>>[] includeProperties);

        T GetById(int id);
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        void Delete(int id);
        void SaveChanges();
    }
}
