﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ostunimekiri.DataModels.Models;

namespace Ostunimekiri.Repos.Contracts
{
    public interface IShoppingListUserRepository : IRepository<ShoppingListUser>
    {
        /// <summary>
        /// Tagastab ostunimekirja kasutaja 
        /// </summary>
        /// <param name="shoppingListUserId">LOstunimekirja kasutaja ID</param>
        /// <returns>Ostunimekirja kasutaja</returns>
        ShoppingListUser GetShoppingListUser(int shoppingListUserId);

        /// <summary>
        /// Tagastab listi ostunimekirja kasutajatest
        /// </summary>
        /// <param name="shoppingListId">Ostunimekirja ID</param>
        /// <returns>Ostunimekirja kasutajad</returns>
        List<ShoppingListUser> GetShoppingUser(int shoppingListId);

        /// <summary>
        /// Ostunimekirjaga seotakse uus kasutaja
        /// </summary>
        /// <param name="shoppingListUser">Listi kasutaja</param>
        void AddShoppingListUser(ShoppingListUser shoppingListUser);

        /// <summary>
        /// Muudetakse olemasoleva listiga seotud kasutajat
        /// </summary>
        /// <param name="shoppingListUser">Ostunimekirja kasutaja</param>
        void PutShoppingListUser(ShoppingListUser shoppingListUser);

        /// <summary>
        /// Kustutatakse ostunimekirja kasutaja
        /// </summary>
        /// <param name="shoppingListUserId">Ostunimekirja kasutaja ID</param>
        void MarkShoppingListUserDeleted(int shoppingListUserId);
    }
}
