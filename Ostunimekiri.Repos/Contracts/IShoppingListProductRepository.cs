﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ostunimekiri.DataModels.Models;

namespace Ostunimekiri.Repos.Contracts
{
    public interface IShoppingListProductRepository : IRepository<ShoppingListProduct>
    {
        /// <summary>
        /// Tooted mis on nimekirjas
        /// </summary>
        /// <param name="shoppingListId">ListiId</param>
        /// <returns>List toodetest(ShoppingListProduct)</returns>
        List<ShoppingListProduct> GetShoppingListProducts(int shoppingListId);

        /// <summary>
        /// Lisab uue ostunimekirjas oleva toote
        /// </summary>
        /// <param name="shoppingListProduct">Ostunimekirjas olev toode</param>
        void PostListShoppingListProduct(List<ShoppingListProduct> shoppingListProduct);

        /// <summary>
        /// Toote märkimine võetuks
        /// Toote "IsTaken" väli märgitakse True-ks
        /// </summary>
        /// <param name="productId">TooteId täisarvuna</param>
        /// <param name="shoppingList">Ostunimekiri objektina</param>
        void MarkShoppingListProductTaken(int productId);

        /// <summary>
        /// Muudab olemasolevat ostunimekirjas olevat toodet
        /// </summary>
        /// <param name="shoppingListProduct">Ostunimekirjas olev toode</param>
        void PutShoppingListProduct(ShoppingListProduct shoppingListProduct);

        /// <summary>
        /// Muudetakse listi toodetest
        /// </summary>
        /// <param name="shoppingListProducts">List toodetest</param>
        void PutListOfShoppingListProduct(List<ShoppingListProduct> shoppingListProducts);

        /// <summary>
        /// Kustutab toote vastavast ostunimekirjast
        /// </summary>
        /// <param name="shoppingListProductId">Kututava toote ID</param>
        void RemoveProductFromExistingShoppingList(int shoppingListProductId);
    }
}
