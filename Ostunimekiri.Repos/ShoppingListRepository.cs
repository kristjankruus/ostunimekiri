﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ostunimekiri.DAL;
using Ostunimekiri.DataModels.Models;
using Ostunimekiri.Repos.Contracts;


namespace Ostunimekiri.Repos
{
    /// <summary>
    /// Nimekirjadega seotud asjad.
    /// ÄRA UNUSTA SaveChanges() LÕPUS, RAISK!
    /// </summary>
    public class ShoppingListRepository : EFRepository<ShoppingList>, IShoppingListRepository
    {
        public List<ShoppingList> GetUserShoppingLists(string userName)
        {
            return base.GetAll()
                .Where(z => z.IsDeleted == false)
                .Where(x => x.ShoppingListUsers
                    .Any(y => y.IdentityUser.UserName == userName))
                .ToList();
        }

        public ShoppingList GetShoppingList(int shoppingListId)
        {
            return base.GetById(shoppingListId);
        }


        public List<ShoppingList> GetBetweenDates(string userName, DateTime starTime, DateTime endTime)
        {
            return GetUserShoppingLists(userName).ToList()
                .Where(x => x.Created >= starTime && x.Created <= endTime)
                .ToList();
        }

        public void AddNewShoppingList(ShoppingList shoppingList)
        {
            if (shoppingList.Products != null)
            {
                CheckIfProductExists(shoppingList.Products);
            }
            base.Add(shoppingList);
            base.SaveChanges();
        }

        public void UpdateShoppingList(ShoppingList shoppingList)
        {
            if (shoppingList.Products != null)
            {
                CheckIfProductExists(shoppingList.Products);
            }
            base.Update(shoppingList);
            base.SaveChanges();
        }

        public void MarkShoppingListInActive(int shoppingListId)
        {
            var shoppingList = base.GetById(shoppingListId);
            shoppingList.IsDeleted = true;
            base.Update(shoppingList);
            base.SaveChanges();
        }

        public void CheckIfProductExists(List<ShoppingListProduct> shoppingListProducts)
        {
            ProductRepository _repo = new ProductRepository();
            foreach (var item in shoppingListProducts)
            {
                var product = _repo.GetByName(item.Product.Name);
                if (product != null)
                {
                    item.ProductId = product.ProductId;
                    item.Product = null;
                }
            }
        }
    }
}
