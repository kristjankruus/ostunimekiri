﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ostunimekiri.DataModels.Models;
using Ostunimekiri.Repos.Contracts;

namespace Ostunimekiri.Repos
{
    public class ShopRepository : EFRepository<Shop>, IShopRepository
    {
        public Shop GetShopById(int shopId)
        {
            return base.GetById(shopId);
        }

        public Shop GetShopByName(string shopName)
        {
            return base.GetAll().Where(x => x.IsDeleted == false && x.Name == shopName).FirstOrDefault();
        }

        public void AddShop(Shop shop)
        {
            base.Add(shop);
            base.SaveChanges();
        }

        public void PutShop(Shop shop)
        {
            base.Update(shop);
            base.SaveChanges();
        }
        public void MarkShopDeleted(int shopId)
        {
            var shoppingList = base.GetById(shopId);

            shoppingList.IsDeleted = false;
            base.Update(shoppingList);
            base.SaveChanges();

        }
    }
}
