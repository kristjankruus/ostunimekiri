﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ostunimekiri.DataModels.Models;
using Ostunimekiri.Repos.Contracts;

namespace Ostunimekiri.Repos
{
    public class ShoppingListProductRepository : EFRepository<ShoppingListProduct>, IShoppingListProductRepository
    {

        public List<ShoppingListProduct> GetShoppingListProducts(int shoppingListId)
        {
            return base.GetAll()
                .Where(x => x.IsDeleted == false)
                .Where(x => x.ShoppingListId == shoppingListId)
                .ToList();
        }

        public ShoppingListProduct GetShoppingListProduct(int shoppingListId, int productId)
        {
            return base.GetAll()
                .Where(x => x.IsDeleted == false)
                .Where(x => x.ShoppingListId == shoppingListId)
                .FirstOrDefault(x => x.ShoppingListProductId == productId);
        }

        public void PostListShoppingListProduct(List<ShoppingListProduct> shoppingListProducts)
        {
            foreach (ShoppingListProduct shoppingListProduct in shoppingListProducts)
            {
                CheckIfExists(shoppingListProduct);
                base.Add(shoppingListProduct);
            }
            base.SaveChanges();
        }

        public void PutShoppingListProduct(ShoppingListProduct shoppingListProduct)
        {
            CheckIfExists(shoppingListProduct);
            base.Update(shoppingListProduct);
            base.SaveChanges();
        }

        public void PutListOfShoppingListProduct(List<ShoppingListProduct> shoppingListProducts)
        {
            foreach (var shoppingListProduct in shoppingListProducts)
            {
                CheckIfExists(shoppingListProduct);
                try
                {
                    base.Update(shoppingListProduct);
                }
                catch (Exception eks)
                {
                    System.Diagnostics.Debug.WriteLine(eks);
                }
            }
            base.SaveChanges();
        }

        public void MarkShoppingListProductTaken(int productId)
        {
            ShoppingListProduct markable = base.GetById(productId);
            markable.IsTaken = true;
            base.Update(markable);
            base.SaveChanges();
        }

        public void RemoveProductFromExistingShoppingList(int shoppingListProductId)
        {
            var shoppingListProduct = base.GetById(shoppingListProductId);

            shoppingListProduct.IsDeleted = true;
            base.Update(shoppingListProduct);
            base.SaveChanges();
        }

        public void CheckIfExists(ShoppingListProduct shoppingListProduct)
        {
            ProductRepository repo = new ProductRepository();
            ShopRepository shopRepo = new ShopRepository();
            Product product = repo.GetByName(shoppingListProduct.Product.Name);
            if (product == null)
            {
                repo.Add(shoppingListProduct.Product);
            }
            else
            {
                shoppingListProduct.ProductId = product.ProductId;
                shoppingListProduct.Product = null;
            }

            if (shoppingListProduct.Shop.Name != null)
            {
                Shop shop = shopRepo.GetShopByName(shoppingListProduct.Shop.Name);
                if (shop == null)
                {
                    shopRepo.Add(shoppingListProduct.Shop);
                }
                else
                {
                    shoppingListProduct.ShopId = shop.ShopId;
                    shoppingListProduct.Shop = null;
                }
            }
            else
            {
                shoppingListProduct.Shop = null;
            }
        }
    }
}
