﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ostunimekiri.DataModels.Models;
using Ostunimekiri.Repos.Contracts;

namespace Ostunimekiri.Repos
{
    public class ShoppingListUserRepository : EFRepository<ShoppingListUser>, IShoppingListUserRepository
    {
        public ShoppingListUser GetShoppingListUser(int shoppingListUserId)
        {
            return base.GetById(shoppingListUserId);
        }

        public List<ShoppingListUser> GetShoppingUser(int shoppingListId)
        {
            return base.GetAll().Where(x => x.ShoppingListId == shoppingListId).ToList();
        }

        public void AddShoppingListUser(ShoppingListUser shoppingListUser)
        {
            base.Add(shoppingListUser);
            base.SaveChanges();
        }

        public void PutShoppingListUser(ShoppingListUser shoppingListUser)
        {
            base.Update(shoppingListUser);
            base.SaveChanges();
        }
        public void MarkShoppingListUserDeleted(int shoppingListUserId)
        {
            var shoppingList = base.GetById(shoppingListUserId);

            shoppingList.IsDeleted = false;
            base.Update(shoppingList);
            base.SaveChanges();
        }
    }
}
