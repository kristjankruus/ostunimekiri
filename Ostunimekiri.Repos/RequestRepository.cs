﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ostunimekiri.DataModels.Models;
using Ostunimekiri.Repos.Contracts;

namespace Ostunimekiri.Repos
{
    public class RequestRepository : EFRepository<Request>, IRequestRepository
    {
        public List<Request> GetRequestById(string id)
        {
            return base.GetAll()
                .Where(x => x.IsDeleted == false && x.IsAccepted == false && x.IdentityUserId == id)
                .ToList();
        }

        public List<Request> GetRequestByListId(int id)
        {
            return base.GetAll()
                .Where(x => x.IsDeleted == false && x.IsAccepted == false && x.ShoppingListId == id)
                .ToList();
        }


        public void AddRequests(Request request)
        {
            base.Add(request);
            base.SaveChanges();
        }

        public void AcceptRequest(Request request)
        {
            ShoppingListUserRepository repo = new ShoppingListUserRepository();
            ShoppingListUser user = new ShoppingListUser()
            {
                IdentityUser = request.IdentityUser,
                JoinTime = DateTime.Now,
                ShoppingListId = request.ShoppingListId
            };
            repo.Add(user);
            request.IsAccepted = true;
            base.Update(request);
            base.SaveChanges();
        }

        public void CancelRequest(int requestId)
        {
            Request request = base.GetById(requestId);
            request.IsAccepted = false;
            request.IsDeleted = true;
            base.Update(request);
            base.SaveChanges();
        }
    }
}
