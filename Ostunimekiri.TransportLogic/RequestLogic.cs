﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ostunimekiri.DataModels.Models;
using Ostunimekiri.Repos;
using Ostunimekiri.Repos.Contracts;
using Ostunimekiri.TransportModels;

namespace Ostunimekiri.TransportLogic
{
    public class RequestLogic
    {
        private IRequestRepository _repo;
        private ModelFactory _factory;


        // Konstruktor
        public RequestLogic()
        {
            _repo = new RequestRepository();
            _factory = new ModelFactory();
        }

        /// <summary>
        /// Küsib tagasi kutsed ning teeb neist DTO-d
        /// </summary>
        /// <param name="id">Kasutaja ID</param>
        /// <returns>List kutsetest</returns>
        public List<RequestDTO> GetRequests(string id)
        {
            List<Request> requests = _repo.GetRequestById(id);
            List<RequestDTO> DTOList = new List<RequestDTO>();

            foreach (Request request in requests)
            {
                DTOList.Add(_factory.Request(request));
            }
            return DTOList;
        }

        /// <summary>
        /// Tagastab listi kutsetest ostunimekirja ID järgi
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<RequestDTO> GetListRequests(int id)
        {
            List<Request> requests = _repo.GetRequestByListId(id);
            List<RequestDTO> DTOList = new List<RequestDTO>();

            foreach (Request request in requests)
            {
                DTOList.Add(_factory.Request(request));
            }
            return DTOList;
        }
    }
}
