﻿using Ostunimekiri.Repos;
using Ostunimekiri.Repos.Contracts;
using Ostunimekiri.TransportModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ostunimekiri.TransportLogic
{
    public class ShoppingListProductLogic
    {
        private IShoppingListProductRepository _repo;
        private ModelFactory _factory;

        //Kontruktor
        public ShoppingListProductLogic(IShoppingListProductRepository repo)
        {
            this._repo = repo;
            this._factory = new ModelFactory();
        }

        /// <summary>
        /// Tagastab listi ostunimekirjas olevatest toodetest
        /// </summary>
        /// <param name="shoppingListID">ostunimekirja ID</param>
        /// <returns>Ostunimekirjas olevad tooted</returns>
        public List<ShoppingListProductDTO> GetShoppingListProductDto(int shoppingListID)
        {
            return _repo.GetShoppingListProducts(shoppingListID)
                .ToList().Select(x => _factory.ShoppingListProduct(x)).ToList();
        }
    }
}
