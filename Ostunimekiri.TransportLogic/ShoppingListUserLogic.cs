﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ostunimekiri.DataModels.Models;
using Ostunimekiri.Repos;
using Ostunimekiri.Repos.Contracts;
using Ostunimekiri.TransportModels;

namespace Ostunimekiri.TransportLogic
{
    public class ShoppingListUserLogic
    {
        private IShoppingListUserRepository _repo;
        private ModelFactory _factory;

        //Kontruktor
        public ShoppingListUserLogic()
        {
            _repo = new ShoppingListUserRepository();
            _factory = new ModelFactory();
        }

        /// <summary>
        /// Tagstab listi ostunimekirja kasutajatest
        /// </summary>
        /// <param name="id">Ostunimekirja kasutaja ID</param>
        /// <returns>List ostunimekirja kasutajatest</returns>
        public List<ShoppingListUserDTO> GetUsers(int id)
        {
            List<ShoppingListUser> requests = _repo.GetShoppingUser(id);
            List<ShoppingListUserDTO> DTOList = new List<ShoppingListUserDTO>();

            DTOList = _factory.ShoppingListUserDto(requests);
            return DTOList;
        }
    }
}
