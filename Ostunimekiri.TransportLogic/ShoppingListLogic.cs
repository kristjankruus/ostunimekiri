﻿using Microsoft.AspNet.Identity.EntityFramework;
using Ostunimekiri.DataModels.Models;
using Ostunimekiri.Repos;
using Ostunimekiri.Repos.Contracts;
using Ostunimekiri.TransportModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ostunimekiri.TransportLogic
{
    public class ShoppingListLogic
    {
        private IShoppingListRepository _repo;
        private ModelFactory _factory;

        //Konstruktor
        public ShoppingListLogic(IShoppingListRepository repo)
        {
            this._repo = repo;
            this._factory = new ModelFactory();
        }

        /// <summary>
        /// Tagatsab listi ostunimekirjadest
        /// </summary>
        /// <param name="userName">Kasutaja nimi</param>
        /// <returns>List ostunimekirjadest</returns>
        public List<ShoppingListDTO> GetShoppingList(string userName)
        {
            List<ShoppingList> shoppingLists = _repo.GetUserShoppingLists(userName);

            List<ShoppingListDTO> DTOList = new List<ShoppingListDTO>();

            foreach (ShoppingList shoppingList in shoppingLists)
            {
                DTOList.Add(_factory.ShoppingList(shoppingList));
            }
            return DTOList;
        }

        /// <summary>
        /// Tagstab põhjaliku ostunimekirja
        /// </summary>
        /// <param name="shoppingListId">Ostunimekirja ID</param>
        /// <returns>Ostunimekiri</returns>
        public ShoppingListDTOBig GetShoppingListDtoBig(int shoppingListId)
        {
            ShoppingList shoppingList = _repo.GetShoppingList(shoppingListId);

            return _factory.ShoppingListBig(shoppingList);
        }
    }
}
