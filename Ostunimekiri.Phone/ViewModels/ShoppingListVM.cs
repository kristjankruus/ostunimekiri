﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ostunimekiri.Phone.Models;
using Ostunimekiri.Phone.Services;

namespace Ostunimekiri.Phone.ViewModels
{
    public class ShoppingListVM
    {
        private ObservableCollection<ShoppingList> _shoppingLists;
        private int _requests;
        private ShoppingListService service = new ShoppingListService();

        public int Requests
        {
            get { return _requests; }
        }

        public ObservableCollection<ShoppingList> ShoppingLists
        {
            get { return _shoppingLists; }
            set { _shoppingLists = value; }
        }

        public ShoppingListVM()
        {
            this._shoppingLists = new ObservableCollection<ShoppingList>();
        }

        public async Task LoadItems()
        {
            await LoadRequests();
            List<ShoppingList> lists = await service.getAllItems(IsolatedStorageSettings.ApplicationSettings["userName"] as string);
            foreach (ShoppingList shoppingList in lists)
            {
                _shoppingLists.Add(shoppingList);
            }
        }

        public async Task LoadRequests()
        {
            RequestService requestService = new RequestService();
            _requests = await requestService.GetRequestCount(IsolatedStorageSettings.ApplicationSettings["guid"] as string);
        }

        internal async Task DeleteLists()
        {
            List<int> lists = new List<int>();

            for (int i = 0; i < _shoppingLists.Count; i++)
            {
                if (_shoppingLists[i].IsDeleted)
                {
                    lists.Add(_shoppingLists[i].ShoppingListId);
                }
            }
            if (await service.deleteItem(lists))
            {
                foreach (int list in lists)
                {
                    _shoppingLists.Remove(_shoppingLists.FirstOrDefault(x => x.ShoppingListId == list));
                }
            }
        }
    }
}
