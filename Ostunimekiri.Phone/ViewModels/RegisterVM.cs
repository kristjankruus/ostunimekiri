﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Phone.UserData;
using Ostunimekiri.Phone.Services;
using Ostunimekiri.Phone.Models;

namespace Ostunimekiri.Phone.ViewModels
{
    public class RegisterVM
    {
        /// <summary>
        /// Registeerib uue kasutaja
        /// </summary>
        /// <param name="username">Kasutajanimi</param>
        /// <param name="password">Parool</param>
        /// <param name="repassword">Parool uuesti</param>
        /// <returns>Kas õnnestus registeerida või mitte</returns>
        public async Task<bool> postAccount(string username, string password, string repassword)
        {
            if (password != repassword)
            {
                System.Windows.MessageBox.Show("Paroolid ei klapi");
                return false;
            }
            else if (username.Equals("") || password.Equals("") || repassword.Equals(""))
            {
                System.Windows.MessageBox.Show("Osad väljad on täitmata");
                return false;
            }
            else if (password.Length < 6)
            {
                System.Windows.MessageBox.Show("Parooli pikkus peab olema vähemalt 6 tähemärki");
                return false;
            }
            else
            {
                Ostunimekiri.Phone.Models.Account acc = new Ostunimekiri.Phone.Models.Account()
                {
                    UserName = username,
                    Password = password,
                    ConfirmPassword = repassword
                };
                AccountService service = new AccountService();
                var test = await service.postItem(acc);
                if (test.StatusCode == HttpStatusCode.BadRequest)
                {
                    System.Windows.MessageBox.Show("Selline kasutaja juba eksisteerib");
                    return false;
                }
                if (test.IsSuccessStatusCode != true)
                {
                    System.Windows.MessageBox.Show("Viga ühendusega");
                    return false;
                }

                System.Windows.MessageBox.Show("Registeeritud");
                return true;
            }
        }
    }
}
