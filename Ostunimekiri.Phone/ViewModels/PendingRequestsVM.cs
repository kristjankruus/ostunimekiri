﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ostunimekiri.Phone.Models;
using Ostunimekiri.Phone.Services;

namespace Ostunimekiri.Phone.ViewModels
{
    public class PendingRequestsVM
    {
        private ObservableCollection<Request> _requests;
        private RequestService service;

        public ObservableCollection<Request> Requests
        {
            get { return _requests; }
            set { _requests = value; }
        }

        public PendingRequestsVM()
        {
            _requests = new ObservableCollection<Request>();
            service = new RequestService();
        }

        /// <summary>
        /// Laeb listi saadetud kutsetest
        /// </summary>
        /// <returns>Saadetud kutsed</returns>
        public async Task LoadRequests()
        {
            List<Request> requests = await service.GetRequests(IsolatedStorageSettings.ApplicationSettings["guid"] as string);
            foreach (Request req in requests)
            {
                _requests.Add(req);
            }
        }

        /// <summary>
        /// Võtab kutse vastu
        /// </summary>
        /// <param name="request">Saadetud kutse</param>
        /// <returns></returns>
        public async Task AcceptRequest(Request request)
        {
            ShoppingListUserService userService = new ShoppingListUserService();
            if (await service.AcceptRequest(request))
            {
                if (await userService.postShoppingListUser(new ShoppingListUser()
                {
                    JoinTime = DateTime.Now,
                    IdentityUserId = IsolatedStorageSettings.ApplicationSettings["guid"].ToString(),
                    ShoppingListId = request.ShoppingListId
                }))
                {
                    _requests.Remove(request);
                    System.Windows.MessageBox.Show("Kutse vastu võetud");
                }
            }
        }

        /// <summary>
        /// Tühistab kutse
        /// </summary>
        /// <param name="request">Saadetud kutse</param>
        /// <returns></returns>
        public async Task CancelRequest(Request request)
        {
            if (await service.CancelRequest(request.RequestId))
            {
                _requests.Remove(request);
                System.Windows.MessageBox.Show("Kustutatud");
            }
        }
    }
}
