﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Phone.Controls.Primitives;
using Ostunimekiri.Phone.Models;
using Ostunimekiri.Phone.Services;

namespace Ostunimekiri.Phone.ViewModels
{
    public class RequestPageVM
    {
        private ObservableCollection<string> _users;
        private ObservableCollection<Request> _requests;
        private AccountService service;

        public ObservableCollection<Request> Requests
        {
            get { return _requests; }
            set { _requests = value; }
        }
        public ObservableCollection<string> ShoppingListUsers
        {
            get { return _users; }
            set { _users = value; }
        }
        //Konstruktor
        public RequestPageVM()
        {
            _users = new ObservableCollection<string>();
            _requests = new ObservableCollection<Request>();
            service = new AccountService();
        }

        /// <summary>
        /// Lae list kutsetest
        /// </summary>
        /// <param name="userName">Kasutajanimi</param>
        /// <returns></returns>
        public async Task LoadItems(string userName)
        {
            _users.Clear();
            List<string> users = await service.userNames(userName);

            foreach (string user in users)
            {
                if (!(user.Equals(IsolatedStorageSettings.ApplicationSettings["userName"] as string)))
                {
                    _users.Add(user);
                }
            }
        }

        /// <summary>
        /// Lae list kutsetest
        /// </summary>
        /// <param name="id">Ostunimekirja ID</param>
        /// <returns></returns>
        public async Task LoadRequests(int id)
        {
            RequestService reqService = new RequestService();
            List<Request> requests = await reqService.GetListRequests(id);
            foreach (Request request in requests)
            {
                _requests.Add(request);
            }
        }

        /// <summary>
        /// Lisab ostunimekirjale uue kutse
        /// </summary>
        /// <param name="userName">Kasutajanimi</param>
        /// <param name="shoppingListId">Ostunimekirja ID</param>
        /// <returns></returns>
        public async Task AddRequestToList(string userName, int shoppingListId)
        {
            IdentityUser user = await service.getUser(userName);
            _requests.Add(new Request()
            {
                ShoppingListId = shoppingListId,
                IdentityUserId = user.Id,
                IdentityUser = user,
                InvitorName = IsolatedStorageSettings.ApplicationSettings["userName"] as string,
                RequestTime = DateTime.Now
            });
        }

        /// <summary>
        /// Salvesta listisolevad kutsed
        /// </summary>
        /// <returns></returns>
        public async Task SaveRequests()
        {
            RequestService requestService = new RequestService();
            List<Request> requests = _requests.ToList().Where(x => x.RequestId == 0).ToList();
            foreach (var request in requests)
            {
                request.IdentityUser = null;
            }
            if (await requestService.PostRequest(requests))
            {
                System.Windows.MessageBox.Show("Salvestatud");
            }
        }

        /// <summary>
        /// kustuta listist kutse
        /// </summary>
        /// <returns></returns>
        public async Task DeleteRequest()
        {
            RequestService reqService = new RequestService();
            List<int> requests = new List<int>();
            for (int i = 0; i < _requests.Count; i++)
            {
                if (_requests[i].IsDeleted)
                {
                    if (_requests[i].RequestId != 0)
                    {
                        requests.Add(_requests[i].RequestId);
                    }
                    _requests.RemoveAt(i);
                    i = i - 1;
                }
            }
            await reqService.DeleteRequest(requests);
        }
    }
}
