﻿using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ostunimekiri.Phone.Models;
using Ostunimekiri.Phone.Services;

namespace Ostunimekiri.Phone.ViewModels
{
    public class StatisticsVM
    {
        private List<ShoppingList> _lists;

        public List<ShoppingList> ShoppingLists
        {
            get { return _lists; }
        }

        public StatisticsVM()
        {
            _lists = new List<ShoppingList>();
        }

        public async Task LoadItems(DateTime startTime, DateTime endTime)
        {
            ShoppingListService service = new ShoppingListService();
            _lists = await service.getBetweenDates(startTime, endTime, IsolatedStorageSettings.ApplicationSettings["userName"].ToString());
        }
    }
}
