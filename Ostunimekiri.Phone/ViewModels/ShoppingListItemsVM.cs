﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Threading.Tasks;
using Ostunimekiri.Phone.Resources;
using Ostunimekiri.Phone.Models;
using Ostunimekiri.Phone.Services;


namespace Ostunimekiri.Phone.ViewModels
{
    public class ShoppingListItemsVM : INotifyPropertyChanged
    {
        private ObservableCollection<ShoppingListProduct> _items;

        public ObservableCollection<ShoppingListProduct> ShoppingList
        {
            get { return _items; }
            set { _items = value; }
        }

        public ShoppingListItemsVM()
        {
            this._items = new ObservableCollection<ShoppingListProduct>();
        }

        public void AddProduct(string result)
        {
            _items.Insert(0, new ShoppingListProduct()
            {
                Product = new Product()
                {
                    Name = result
                }
            });
        }

        public async Task AddShoppingList(string name)
        {
            ShoppingListService service = new ShoppingListService();
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
            for (int i = 0; i < _items.Count; i++)
            {
                if (_items[i].Product.Name.Equals(""))
                {
                    _items.Remove(_items[i]);
                }
            }

            await service.postItem(new ShoppingList()
            {
                Name = name,
                Created = DateTime.Now,
                Products = _items.ToList(),
                IsActive = true,
                ShoppingListUsers = new List<ShoppingListUser>()
                {
                    new ShoppingListUser()
                    {
                        IdentityUserId = settings["guid"] as string,
                        JoinTime = DateTime.Now
                    }
                },
                Requests = new List<Request>()
            });
        }

        public void DeleteProducts()
        {
            for (int i = 0; i < _items.Count; i++)
            {
                if (_items[i].IsDeleted)
                {
                    _items.Remove(_items[i]);
                    i = i - 1;
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}