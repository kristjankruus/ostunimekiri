﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ostunimekiri.Phone.Models;
using Ostunimekiri.Phone.Services;

namespace Ostunimekiri.Phone.ViewModels
{
    public class EditShoppingListVM
    {
        private ObservableCollection<ShoppingListProduct> _products;
        private List<ShoppingListProduct> _oldProducts;
        private List<ShoppingListProduct> _addProducts;
        private ObservableCollection<string> _newProduct;
        private ObservableCollection<string> _shops;
        private ShoppingListProductService _service;
        private double _sum;

        public double Sum
        {
            get { return _sum; }
            set { _sum = value; }
        }

        public ObservableCollection<ShoppingListProduct> ShoppingListProducts
        {
            get { return _products; }
            set { _products = value; }
        }

        //Kontruktor
        public EditShoppingListVM()
        {
            _products = new ObservableCollection<ShoppingListProduct>();
            _oldProducts = new List<ShoppingListProduct>();
            _addProducts = new List<ShoppingListProduct>();
            _newProduct = new ObservableCollection<string>();
            _shops = new ObservableCollection<string>();
            _service = new ShoppingListProductService();
            _sum = new double();
        }

        /// <summary>
        /// Lae uued ostunimekirja tooted listi
        /// </summary>
        /// <param name="shoppingListId">Ostunimekirja ID</param>
        /// <returns>List ostunimekirja toodetest</returns>
        public async Task LoadProducts(int shoppingListId)
        {
            List<ShoppingListProduct> products = await _service.getById(shoppingListId);
            foreach (ShoppingListProduct shoppingListProduct in products)
            {
                _products.Add(shoppingListProduct);
                _oldProducts.Add(shoppingListProduct);
            }
            FindSum();
        }

        /// <summary>
        /// Lisab uue toote ostunimekirja
        /// </summary>
        /// <param name="result">Toote nimetus</param>
        /// <param name="shoppingListId">Ostunimekirja ID</param>
        public void AddProduct(string result, int shoppingListId)
        {
            ShoppingListProduct product = new ShoppingListProduct()
            {
                ShoppingListId = shoppingListId,
                Product = new Product()
                {
                    Name = result
                },
                Shop = new Shop()
            };
            _products.Add(product);
            _addProducts.Add(product);
            FindSum();
        }

        /// <summary>
        /// Leiab ostunimekirja toodete kogumaksumuse
        /// </summary>
        public void FindSum()
        {
            foreach (ShoppingListProduct shoppingListProduct in _products)
            {
                _sum += (shoppingListProduct.Quantity * shoppingListProduct.Price);
            }
        }

        /// <summary>
        /// Uuendab olemasoleva ostunimekirja tooteid
        /// </summary>
        /// <returns></returns>
        public async Task UpdateShoppingList()
        {
            bool test = true;
            bool test2 = true;
            if (_addProducts.Count > 0)
            {
                test = await _service.postItem(_addProducts);
            }

            if (_oldProducts.Count > 0)
            {
                test2 = await _service.putItem(_oldProducts);
            }
            if (test == true && test2 == true)
            {
                System.Windows.MessageBox.Show("Salvestatud");
            }
            FindSum();
        }

        /// <summary>
        /// Kustutab olemasolevast ostunimekirjast listi toodetest
        /// </summary>
        /// <returns></returns>
        public async Task RemoveProducts()
        {
            List<int> removeProducts = new List<int>();
            foreach (var shoppingListProduct in _products)
            {
                if (shoppingListProduct.IsDeleted && shoppingListProduct.ShoppingListProductId != 0)
                {
                    removeProducts.Add(shoppingListProduct.ShoppingListProductId);
                }
                else
                {
                    _products.Remove(shoppingListProduct);
                }
            }
            if (await _service.deleteProduct(removeProducts))
            {
                for (int i = 0; i < _products.Count; i++)
                {
                    if (_products[i].IsDeleted)
                    {
                        _products.Remove(_products[i]);
                        _oldProducts.Remove(_oldProducts[i]);
                        i = i - 1;
                    }
                }
                System.Windows.MessageBox.Show("Kustutatud");
            }
            FindSum();
        }
    }
}
