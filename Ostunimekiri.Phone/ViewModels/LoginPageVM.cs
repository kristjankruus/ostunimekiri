﻿using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Ostunimekiri.Phone.Models;
using Ostunimekiri.Phone.Services;

namespace Ostunimekiri.Phone.ViewModels
{
    public class LoginPageVM
    {
        /// <summary>
        /// Logib sisse vastav kasutaja nime all
        /// </summary>
        /// <param name="userName">Kasutajanimi</param>
        /// <param name="password">Parool</param>
        /// <returns></returns>
        public async Task<bool> Login(string userName, string password)
        {
            LoginService login = new LoginService();
            var authorize = await login.authenticate(userName, password);

            if (authorize.IsSuccessStatusCode == true)
            {
                IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
                //Salvestatakse token IsolatedStoragesse
                var token = authorize.Content.ReadAsStringAsync().Result.Substring(17).Remove(427);
                if (!settings.Contains("userData"))
                {
                    settings.Add("userData", token);
                }
                else
                {
                    settings["userData"] = token;
                }

                if (!settings.Contains("userName"))
                {
                    settings.Add("userName", userName);
                }
                else
                {
                    settings["userName"] = userName;
                }
                settings.Save();
                //Küsitakse kasutaja GUID
                string guidToken = await login.guid(userName);
                if (!settings.Contains("guid"))
                {
                    settings.Add("guid", guidToken);
                }
                else
                {
                    settings["guid"] = guidToken;
                }
                settings.Save();
                return true;
            }
            else if (authorize.StatusCode == HttpStatusCode.NotFound)
            {
                System.Windows.MessageBox.Show("Viga ühendusega");
                return false;
            }
            else
            {
                System.Windows.MessageBox.Show("Kasutajanimi või parool on vale");
                return false;
            }

        }
    }
}
