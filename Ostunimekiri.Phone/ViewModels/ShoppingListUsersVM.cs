﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ostunimekiri.Phone.Models;
using Ostunimekiri.Phone.Services;

namespace Ostunimekiri.Phone.ViewModels
{
    public class ShoppingListUsersVM
    {
        private ObservableCollection<ShoppingListUser> _users;
        private ShoppingListUserService service;

        public ObservableCollection<ShoppingListUser> ShoppingListUsers
        {
            get { return _users; }
            set { _users = value; }
        }

        public ShoppingListUsersVM()
        {
            _users = new ObservableCollection<ShoppingListUser>();
            service = new ShoppingListUserService();
        }
        public async Task LoadUsers(int id)
        {
            List<ShoppingListUser> users = await service.getShoppingList(id);
            foreach (ShoppingListUser shoppingListUser in users)
            {
                _users.Add(shoppingListUser);
            }
        }
    }
}
