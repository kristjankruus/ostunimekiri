﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ostunimekiri.Phone.Models
{
    public class IdentityUser
    {
        public string Id { get; set; }
        public string UserName { get; set; }
    }
}
