﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ostunimekiri.Phone.Models
{
    public class ShoppingListProduct
    {
        public int ShoppingListProductId { get; set; }
        public int ShoppingListId { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public int? ShopId { get; set; }
        public Shop Shop { get; set; }
        public double Quantity { get; set; }
        public double Price { get; set; }
        public bool IsTaken { get; set; }
        public bool IsDeleted { get; set; }
    }
}
