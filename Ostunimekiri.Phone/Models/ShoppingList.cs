﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ostunimekiri.Phone.Models
{
    public class ShoppingList
    {
        public int ShoppingListId { get; set; }
        public string Name { get; set; }
        public DateTime Created { get; set; }
        public bool IsActive { get; set; }
        public List<ShoppingListProduct> Products { get; set; }
        public List<Request> Requests { get; set; }
        public List<ShoppingListUser> ShoppingListUsers { get; set; }
        public bool IsDeleted { get; set; }
    }
}
