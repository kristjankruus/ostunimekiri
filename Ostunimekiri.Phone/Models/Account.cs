﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Ostunimekiri.Phone.Models
{
    public class Account
    {
        private string _userName;
        private string _password;
        private string _repassword;

        public string ConfirmPassword
        {
            get { return _repassword; }
            set
            {
                _repassword = value;
            }

        }

        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
            }
        }


        public string UserName
        {
            get { return _userName; }
            set
            {
                _userName = value;
            }
        }
    }
}
