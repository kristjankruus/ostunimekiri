﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ostunimekiri.Phone.Models
{
    public class ShoppingListUser
    {
        public int ShoppingListUserId { get; set; }
        public string IdentityUserId { get; set; }
        public string IdentityUserName { get; set; }
        public int ShoppingListId { get; set; }
        public DateTime JoinTime { get; set; }
        public bool IsDeleted { get; set; }
    }
}
