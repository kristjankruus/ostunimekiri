﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Ostunimekiri.Phone.Models;
using Ostunimekiri.Phone.Resources;
using Ostunimekiri.Phone.ViewModels;
using GestureEventArgs = System.Windows.Input.GestureEventArgs;

namespace Ostunimekiri.Phone
{
    public partial class MainPage : PhoneApplicationPage
    {
        private ShoppingListVM vm;

        public MainPage()
        {
            InitializeComponent();
            this.Loaded += MainPage_Loaded;
        }

        async void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            vm = new ShoppingListVM();
            await vm.LoadItems();
            DataContext = vm;
        }

        private void ApplicationBarIconButton_OnClick(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/ShoppingListItems.xaml", UriKind.Relative));
        }
        private void UIElement_OnTap(object sender, GestureEventArgs e)
        {
            var listId = (((sender as TextBlock).DataContext) as ShoppingList).ShoppingListId;
            NavigationService.Navigate(new Uri("/EditShoppingList.xaml?shoppingListId=" + listId, UriKind.Relative));
        }

        private void StatisticsButton_OnClick(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/StatisticsPage.xaml", UriKind.Relative));
        }

        private async void DeleteButton_OnClick(object sender, EventArgs e)
        {
            if (DeleteBox.Visibility != Visibility.Visible)
            {
                DeleteBox.Visibility = Visibility.Visible;
                EnableButton(false);
            }
            else
            {
                await vm.DeleteLists();
                DeleteBox.Visibility = Visibility.Collapsed;
                EnableButton(true);
            }
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            if (DeleteBox.Visibility == Visibility.Collapsed)
            {
                MessageBoxResult mRes = MessageBox.Show("", "Logi välja?", MessageBoxButton.OKCancel);
                if (mRes == MessageBoxResult.OK)
                {
                    NavigationService.GoBack();
                }
                if (mRes == MessageBoxResult.Cancel)
                {
                    e.Cancel = true;
                }
            }
            else
            {
                DeleteBox.Visibility = Visibility.Collapsed;
                EnableButton(true);
                e.Cancel = true;
            }
        }

        private void ToggleButton_OnChecked(object sender, RoutedEventArgs e)
        {
            ((sender as CheckBox).DataContext as ShoppingList).IsDeleted = true;
        }

        private void EnableButton(bool enable)
        {
            ApplicationBarIconButton b = (ApplicationBarIconButton)ApplicationBar.Buttons[0];
            b.IsEnabled = enable;
            ApplicationBarIconButton b2 = (ApplicationBarIconButton)ApplicationBar.Buttons[1];
            b2.IsEnabled = enable;
        }

        private void RequestTextBlock_OnTap(object sender, GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/PendingRequestPage.xaml", UriKind.Relative));
        }
    }
}