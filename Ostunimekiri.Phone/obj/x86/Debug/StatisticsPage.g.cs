﻿#pragma checksum "C:\Users\KristjanOliver\Desktop\Ostunimekiri\Ostunimekiri.Phone\StatisticsPage.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "BC179A176335C9D6F4EC981DDB37D910"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.Phone.Controls;
using Sparrow.Chart;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace Ostunimekiri.Phone {
    
    
    public partial class StatisticsPage : Microsoft.Phone.Controls.PhoneApplicationPage {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal Microsoft.Phone.Controls.DatePicker StartTime;
        
        internal Microsoft.Phone.Controls.DatePicker EndTime;
        
        internal Microsoft.Phone.Controls.ListPicker ProductPicker;
        
        internal System.Windows.Controls.Grid ContentPanel;
        
        internal Sparrow.Chart.SparrowChart test;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/Ostunimekiri.Phone;component/StatisticsPage.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.StartTime = ((Microsoft.Phone.Controls.DatePicker)(this.FindName("StartTime")));
            this.EndTime = ((Microsoft.Phone.Controls.DatePicker)(this.FindName("EndTime")));
            this.ProductPicker = ((Microsoft.Phone.Controls.ListPicker)(this.FindName("ProductPicker")));
            this.ContentPanel = ((System.Windows.Controls.Grid)(this.FindName("ContentPanel")));
            this.test = ((Sparrow.Chart.SparrowChart)(this.FindName("test")));
        }
    }
}

