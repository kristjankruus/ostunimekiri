﻿using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using HttpClientExtras;

namespace Ostunimekiri.Phone.Services
{
    public abstract class BaseService
    {
        private string _baseAddress;
        private string _authToken;

        public BaseService(string baseAddress)
        {
            this._baseAddress = baseAddress;
            if (IsolatedStorageSettings.ApplicationSettings.Contains("userData"))
            {
                this._authToken = IsolatedStorageSettings.ApplicationSettings["userData"] as string;
            }
        }

        public async Task<T> GetServiceAsync<T>(string path)
        {
            using (HttpClient client = new HttpClient(new HttpClientHandler(), true))
            {
                client.BaseAddress = new Uri(_baseAddress);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _authToken);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage resp = await client.GetAsync(path);
                T ret = await resp.Content.ReadAsAsync<T>();
                return ret;
            }
        }
        public async Task<HttpResponseMessage> PostServiceAsync<T>(string path, T item)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_baseAddress);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _authToken);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage resp = await client.PostAsJsonAsync(path, item);
                return resp;
            }
        }
        public async Task<HttpResponseMessage> PutServiceAsync<T>(string path, T item)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_baseAddress);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _authToken);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage resp = await client.PutAsJsonAsync(path, item);
                return resp;
            }
        }

        public async Task<HttpResponseMessage> DeleteServiceAsync<T>(string path, int id)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_baseAddress);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _authToken);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage resp = await client.DeleteAsync(path + id);
                return resp.EnsureSuccessStatusCode();
            }
        }
    }
}
