﻿using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Ostunimekiri.Phone.Models;

namespace Ostunimekiri.Phone.Services
{
    public class LoginService
    {
        internal async Task<HttpResponseMessage> authenticate(string userName, string password)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:10833/");
                HttpContent content = new StringContent("grant_type=password&username=" + userName + "&password=" + password);
                content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
                HttpResponseMessage test = await client.PostAsync("Token", content);
                return test;
            }
        }


        internal async Task<string> guid(string userName)
        {
            using (HttpClient client = new HttpClient())
            {
                IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", IsolatedStorageSettings.ApplicationSettings["userData"] as string);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage test = await client.GetAsync("http://localhost:10833/api/Account/GetGUID?userName=" + userName);
                string ret = await test.Content.ReadAsAsync<string>();
                return ret;
            }
        }
    }
}
