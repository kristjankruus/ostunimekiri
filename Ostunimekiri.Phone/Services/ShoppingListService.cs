﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Ostunimekiri.Phone.Models;

namespace Ostunimekiri.Phone.Services
{
    public class ShoppingListService : BaseService
    {
        public ShoppingListService()
            : base("http://localhost:10833/api/")
        {

        }

        public async Task<List<ShoppingList>> getAllItems(string userName)
        {
            return await base.GetServiceAsync<List<ShoppingList>>("ShoppingList?userName=" + userName);
        }

        public async Task<ShoppingList> getShoppingList(int shoppingListId)
        {
            return await base.GetServiceAsync<ShoppingList>("ShoppingList?shoppingListId=" + shoppingListId);
        }

        public async Task postItem(ShoppingList shoppingList)
        {
            HttpResponseMessage resp = await base.PostServiceAsync<ShoppingList>("ShoppingList", shoppingList);
            if (resp.IsSuccessStatusCode != true)
            {
                System.Windows.MessageBox.Show("Viga");
            }
        }

        public async Task<List<ShoppingList>> getBetweenDates(DateTime startTime, DateTime endTime, string userName)
        {
            return await base.GetServiceAsync<List<ShoppingList>>("ShoppingList?userName=" + userName + "&startTime=" + startTime + "&endTime=" + endTime);
        }

        public async Task putItem(List<ShoppingListProduct> shoppingListProducts)
        {
            HttpResponseMessage resp = await base.PutServiceAsync<List<ShoppingListProduct>>("ShoppingListProduct", shoppingListProducts);
            if (resp.IsSuccessStatusCode != true)
            {
                System.Windows.MessageBox.Show("Viga");
            }
            else
            {
                System.Windows.MessageBox.Show("Salvestatud");
            }
        }

        public async Task<bool> deleteItem(List<int> shoppingListIds)
        {
            foreach (int shoppingListId in shoppingListIds)
            {
                HttpResponseMessage resp = await base.DeleteServiceAsync<ShoppingList>("ShoppingList?shoppingListId=", shoppingListId);
                if (resp.IsSuccessStatusCode != true)
                {
                    System.Windows.MessageBox.Show("Viga");
                    return false;
                }
            }
            return true;
        }
    }
}
