﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Ostunimekiri.Phone.Models;

namespace Ostunimekiri.Phone.Services
{
    public class ShoppingListProductService : BaseService
    {
        public ShoppingListProductService()
            : base("http://localhost:10833/api/")
        {

        }
        public async Task<List<ShoppingListProduct>> getAllItems()
        {
            List<ShoppingListProduct> ret = await base.GetServiceAsync<List<ShoppingListProduct>>("ShoppingListProduct");
            return ret;
        }
        public async Task<List<ShoppingListProduct>> getById(int id)
        {
            List<ShoppingListProduct> ret = await base.GetServiceAsync<List<ShoppingListProduct>>("ShoppingListProduct?shoppingListId=" + id);
            return ret;
        }
        public async Task<bool> postItem(List<ShoppingListProduct> products)
        {
            HttpResponseMessage resp = await base.PostServiceAsync<List<ShoppingListProduct>>("ShoppingListProduct", products);
            if (resp.IsSuccessStatusCode != true)
            {
                System.Windows.MessageBox.Show("Viga");
                return false;
            }
            return true;
        }
        public async Task<bool> putItem(List<ShoppingListProduct> products)
        {
            HttpResponseMessage resp = await base.PutServiceAsync<List<ShoppingListProduct>>("ShoppingListProduct", products);
            if (resp.IsSuccessStatusCode != true)
            {
                System.Windows.MessageBox.Show("Viga");
                return false;
            }
            return true;
        }

        public async Task<bool> deleteProduct(List<int> requests)
        {
            foreach (int request in requests)
            {
                HttpResponseMessage resp = await DeleteServiceAsync<ShoppingListProduct>("ShoppingListProduct/", request);
                if (resp.IsSuccessStatusCode != true)
                {
                    System.Windows.MessageBox.Show("Viga");
                    return false;
                }
            }
            return true;
        }
    }
}
