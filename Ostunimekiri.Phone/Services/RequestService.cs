﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Ostunimekiri.Phone.Models;

namespace Ostunimekiri.Phone.Services
{
    public class RequestService : BaseService
    {
        public RequestService()
            : base("http://localhost:10833/api/")
        {

        }

        public async Task<int> GetRequestCount(string userId)
        {
            return await base.GetServiceAsync<int>("Request?userId=" + userId);
        }

        public async Task<List<Request>> GetRequests(string userId)
        {
            return await base.GetServiceAsync<List<Request>>("Request/GetRequests?userId=" + userId);
        }

        public async Task<List<Request>> GetListRequests(int userId)
        {
            return await base.GetServiceAsync<List<Request>>("Request?listId=" + userId);
        }

        public async Task<bool> PostRequest(List<Request> request)
        {
            HttpResponseMessage resp = await PostServiceAsync("Request", request);
            if (resp.IsSuccessStatusCode != true)
            {
                System.Windows.MessageBox.Show("Viga");
                return false;
            }
            return true;
        }

        public async Task DeleteRequest(List<int> requests)
        {
            foreach (int request in requests)
            {
                HttpResponseMessage resp = await DeleteServiceAsync<Request>("Request", request);
                if (resp.IsSuccessStatusCode != true)
                {
                    System.Windows.MessageBox.Show("Viga");
                }
            }
        }

        public async Task<bool> AcceptRequest(Request request)
        {
            HttpResponseMessage resp = await PutServiceAsync("Request", request);
            if (resp.IsSuccessStatusCode != true)
            {
                System.Windows.MessageBox.Show("Viga");
                return false;
            }
            return true;
        }
        public async Task<bool> CancelRequest(int requestId)
        {
            HttpResponseMessage resp = await DeleteServiceAsync<Request>("Request/", requestId);
            if (resp.IsSuccessStatusCode != true)
            {
                System.Windows.MessageBox.Show("Viga");
                return false;
            }
            return true;
        }
    }
}
