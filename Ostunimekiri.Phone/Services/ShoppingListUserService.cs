﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Ostunimekiri.Phone.Models;

namespace Ostunimekiri.Phone.Services
{
    public class ShoppingListUserService : BaseService
    {
        public ShoppingListUserService()
            : base("http://localhost:10833/api/")
        {

        }

        public async Task<List<ShoppingListUser>> getShoppingList(int shoppingListId)
        {
            return await GetServiceAsync<List<ShoppingListUser>>("ShoppingListUser/GetUser?id=" + shoppingListId);
        }

        public async Task<bool> postShoppingListUser(ShoppingListUser user)
        {
            HttpResponseMessage resp = await PostServiceAsync("ShoppingListUser", user);
            if (resp.IsSuccessStatusCode != true)
            {
                System.Windows.MessageBox.Show("Viga");
                return false;
            }
            return true;
        }
    }
}
