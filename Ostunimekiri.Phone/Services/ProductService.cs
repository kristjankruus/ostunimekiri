﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Ostunimekiri.Phone.Models;

namespace Ostunimekiri.Phone.Services
{
    public class ProductService : BaseService
    {
        public ProductService()
            : base("http://localhost:10833/api/")
        {

        }

        public async Task<List<string>> getByName(string name)
        {
            List<string> ret = await GetServiceAsync<List<string>>("Product?name=" + name);
            return ret;
        }
    }
}
