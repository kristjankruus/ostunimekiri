﻿using System.IO.IsolatedStorage;
using System.Net.Http.Headers;
using Ostunimekiri.Phone.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;


namespace Ostunimekiri.Phone.Services
{
    public class AccountService : BaseService
    {
        public AccountService()
            : base("http://localhost:10833/api/")
        {

        }
        public async Task<List<Account>> getAllItems()
        {
            List<Account> ret = await base.GetServiceAsync<List<Account>>("Account");
            return ret;
        }
        public async Task<List<Account>> getAllActive()
        {
            List<Account> ret = await base.GetServiceAsync<List<Account>>("Account");
            return ret;
        }
        public async Task<HttpResponseMessage> postItem(Account account)
        {
            HttpResponseMessage resp = await base.PostServiceAsync<Account>("Account/Register", account);
            return resp;
        }

        internal async Task<List<string>> userNames(string userName)
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", IsolatedStorageSettings.ApplicationSettings["userData"] as string);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage test = await client.GetAsync("http://localhost:10833/api/Account/GetName?userName=" + userName);
                List<string> ret = await test.Content.ReadAsAsync<List<string>>();
                return ret;
            }
        }

        internal async Task<IdentityUser> getUser(string name)
        {
            IdentityUser user = await base.GetServiceAsync<IdentityUser>("Account/UserInfo?name=" + name);
            return user;
        }

        internal async Task<string> checkUserName(string userName)
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", IsolatedStorageSettings.ApplicationSettings["userData"] as string);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage test = await client.GetAsync("http://localhost:10833/api/Account/CheckName?userName=" + userName);
                string ret = await test.Content.ReadAsAsync<string>();
                return ret;
            }
        }

        public async Task putItem(Account account)
        {
            HttpResponseMessage resp = await base.PutServiceAsync<Account>("Account", account);
            if (resp.IsSuccessStatusCode != true)
            {
                System.Windows.MessageBox.Show("Viga");
            }
        }
    }
}
