﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Windows.Phone.Speech.Recognition;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Ostunimekiri.Phone.Models;
using Ostunimekiri.Phone.ViewModels;
using GestureEventArgs = System.Windows.Input.GestureEventArgs;

namespace Ostunimekiri.Phone
{
    public partial class ShoppingListItems : PhoneApplicationPage
    {

        private ShoppingListItemsVM vm = new ShoppingListItemsVM();

        public ShoppingListItems()
        {
            InitializeComponent();
            this.Loaded += ShoppingList_Loaded;
        }

        private bool stopped = true;
        private SpeechRecognizerUI recoWithUI;

        private void ShoppingList_Loaded(object sender, RoutedEventArgs e)
        {
            vm.AddProduct("");
            DataContext = vm;
        }

        private async void StartButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (stopped)
            {
                stopped = false;
                recoWithUI = new SpeechRecognizerUI();
                recoWithUI.Settings.ExampleText = "Press BACK to end recognition";
                recoWithUI.Recognizer.Settings.InitialSilenceTimeout = TimeSpan.FromSeconds(2.0);
                recoWithUI.Recognizer.Settings.EndSilenceTimeout = TimeSpan.FromSeconds(0.1);

                SpeechRecognitionUIResult recoResult = await recoWithUI.RecognizeWithUIAsync();
                if (recoResult.ResultStatus != SpeechRecognitionUIStatus.Cancelled)
                {
                    vm.AddProduct(recoResult.RecognitionResult.Text);
                    DataContext = null;
                    ShoppingList_Loaded(sender, e);
                }
                StartButton_OnClick(sender, e);
            }
        }

        private async void ApplicationBarIconButton_OnClick(object sender, EventArgs e)
        {
            await vm.AddShoppingList(HeadingBox.Text);
            NavigationService.GoBack();
        }

        protected override void OnBackKeyPress(CancelEventArgs e)
        {
            if (stopped == false)
            {
                stopped = true;
                recoWithUI.Dispose();
            }
            else
            {
                //  base.OnBackKeyPress(e);
                if (DeleteBox.Visibility == Visibility.Collapsed)
                {
                    NavigationService.GoBack();
                }
                else
                {
                    DeleteBox.Visibility = Visibility.Collapsed;
                    EnableButton(true);
                    e.Cancel = true;
                }
            }
        }

        private void ProductBox_OnTap(object sender, GestureEventArgs e)
        {
            if (ProductListBox.Items.Count < 2)
            {
                vm.AddProduct("");
            }
            else
            {
                string firstText = vm.ShoppingList.ElementAt(0).Product.Name;
                string secText = vm.ShoppingList.ElementAt(1).Product.Name;

                if (!(firstText.Equals(secText)))
                {
                    vm.AddProduct("");
                }
            }
        }

        private void DeleteButton_OnClick(object sender, EventArgs e)
        {
            if (DeleteBox.Visibility == Visibility.Collapsed)
            {
                DeleteBox.Visibility = Visibility.Visible;
                EnableButton(false);
            }
            else
            {
                vm.DeleteProducts();
                if (ProductListBox.Items.Count == 0)
                {
                    vm.AddProduct("");
                }
                DeleteBox.Visibility = Visibility.Collapsed;
                EnableButton(true);
            }
        }

        private void EnableButton(bool enable)
        {
            ApplicationBarIconButton b = (ApplicationBarIconButton)ApplicationBar.Buttons[0];
            b.IsEnabled = enable;
        }

        private void ToggleButton_OnChecked(object sender, RoutedEventArgs e)
        {
            ((sender as CheckBox).DataContext as ShoppingListProduct).IsDeleted = true;
        }
    }
}
