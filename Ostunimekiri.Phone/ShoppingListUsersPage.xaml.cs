﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Ostunimekiri.Phone.ViewModels;

namespace Ostunimekiri.Phone
{
    public partial class ShoppingListUsersPage : PhoneApplicationPage
    {
        private ShoppingListUsersVM vm;
        private string parameter = string.Empty;

        public ShoppingListUsersPage()
        {
            InitializeComponent();
            Loaded += ShoppingListUsersPage_Loaded;
        }

        async void ShoppingListUsersPage_Loaded(object sender, RoutedEventArgs e)
        {
            NavigationContext.QueryString.TryGetValue("shoppingListId", out parameter);

            vm = new ShoppingListUsersVM();
            await vm.LoadUsers(Convert.ToInt32(parameter));
            DataContext = vm;
        }

        private void RequestButton_OnClick(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/RequestPage.xaml?shoppingListId=" + parameter, UriKind.Relative));
        }
    }
}