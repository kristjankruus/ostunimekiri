﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Ostunimekiri.Phone.ViewModels;

namespace Ostunimekiri.Phone
{
    public partial class StatisticsPage : PhoneApplicationPage
    {
        private StatisticsVM vm;
        public StatisticsPage()
        {
            InitializeComponent();
        }

        private async void DateTimePickerBase_OnValueChanged(object sender, DateTimeValueChangedEventArgs e)
        {
            if (StartTime.Value != null && EndTime.Value != null)
            {
                if (EndTime.Value > StartTime.Value)
                {
                    await vm.LoadItems(Convert.ToDateTime(StartTime.Value), Convert.ToDateTime(EndTime.Value));
                    DataContext = vm;
                    ProductPicker.Visibility = Visibility.Visible;
                }
                else
                {
                    MessageBox.Show("Lõpukuupäev on väiksem kui alguskuupäev");
                }
            }
        }
    }
}