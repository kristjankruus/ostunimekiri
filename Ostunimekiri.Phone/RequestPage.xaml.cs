﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Ostunimekiri.Phone.Models;
using Ostunimekiri.Phone.ViewModels;

namespace Ostunimekiri.Phone
{
    public partial class RequestPage : PhoneApplicationPage
    {
        private RequestPageVM vm;
        private string parameter = string.Empty;

        public RequestPage()
        {
            InitializeComponent();
            Loaded += RequestPage_Loaded;
        }
        async void RequestPage_Loaded(object sender, RoutedEventArgs e)
        {
            NavigationContext.QueryString.TryGetValue("shoppingListId", out parameter);

            vm = new RequestPageVM();
            await vm.LoadRequests(Convert.ToInt32(parameter));
            DataContext = vm;
        }

        private async void DeleteButton_OnClick(object sender, EventArgs e)
        {
            if (DeleteBox.Visibility == Visibility.Collapsed)
            {
                DeleteBox.Visibility = Visibility.Visible;
                EnableButton(false);
            }
            else
            {
                await vm.DeleteRequest();
                DeleteBox.Visibility = Visibility.Collapsed;
                EnableButton(true);
            }
        }

        private void EnableButton(bool enable)
        {
            ApplicationBarIconButton b = (ApplicationBarIconButton)ApplicationBar.Buttons[0];
            b.IsEnabled = enable;
        }

        private async void CompleteBox_OnPopulating(object sender, PopulatingEventArgs e)
        {
            await vm.LoadItems(CompleteBox.Text);
            CompleteBox.PopulateComplete();
        }

        private async void CompleteBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CompleteBox.IsEnabled = false;
            await vm.AddRequestToList(CompleteBox.Text, Convert.ToInt32(parameter));
            CompleteBox.IsEnabled = true;
        }

        private async void SaveButton_OnClick(object sender, EventArgs e)
        {
            await vm.SaveRequests();
        }

        private void ToggleButton_OnChecked(object sender, RoutedEventArgs e)
        {
            ((sender as CheckBox).DataContext as Request).IsDeleted = true;
        }
    }
}