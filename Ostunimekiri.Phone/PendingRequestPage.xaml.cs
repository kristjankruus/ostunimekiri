﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Ostunimekiri.Phone.Models;
using Ostunimekiri.Phone.ViewModels;

namespace Ostunimekiri.Phone
{
    public partial class PendingRequestPage : PhoneApplicationPage
    {
        private PendingRequestsVM vm;

        public PendingRequestPage()
        {
            InitializeComponent();
            Loaded += PendingRequestPage_Loaded;
        }

        async void PendingRequestPage_Loaded(object sender, RoutedEventArgs e)
        {
            vm = new PendingRequestsVM();
            await vm.LoadRequests();
            DataContext = vm;
        }

        private async void JoinButton_OnClick(object sender, EventArgs e)
        {
            await vm.AcceptRequest(RequestBox.SelectedItem as Request);
            SetIconVisible(false);
        }

        private async void CancelButton_OnClick(object sender, EventArgs e)
        {
            await vm.CancelRequest(RequestBox.SelectedItem as Request);
            SetIconVisible(false);
        }

        private void RequestBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetIconVisible(true);
        }

        private void SetIconVisible(bool visible)
        {
            ApplicationBarIconButton b = (ApplicationBarIconButton)ApplicationBar.Buttons[0];
            ApplicationBarIconButton b2 = (ApplicationBarIconButton)ApplicationBar.Buttons[1];
            b.IsEnabled = visible;
            b2.IsEnabled = visible;
        }
    }
}