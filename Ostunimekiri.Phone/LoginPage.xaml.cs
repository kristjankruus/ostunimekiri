﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Ostunimekiri.Phone.ViewModels;

namespace Ostunimekiri.Phone
{
    public partial class LoginPage : PhoneApplicationPage
    {
        public LoginPage()
        {
            InitializeComponent();
        }
        private async void loginButton_Click(object sender, RoutedEventArgs e)
        {
            LoginPageVM vm = new LoginPageVM();
            if (await vm.Login(userName.Text, password.Password))
                NavigationService.Navigate(new Uri("/ShoppingList.xaml", UriKind.Relative));
            userName.Text = "";
            password.Password = "";
        }

        private void register_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/RegisterPage.xaml", UriKind.Relative));
        }
    }
}