﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Ostunimekiri.Phone.Models;
using Ostunimekiri.Phone.ViewModels;

namespace Ostunimekiri.Phone
{
    public partial class EditShoppingList : PhoneApplicationPage
    {
        private EditShoppingListVM vm;
        private string parameter = string.Empty;

        public EditShoppingList()
        {
            InitializeComponent();
            this.Loaded += EditShoppingList_Loaded;
        }

        private async void EditShoppingList_Loaded(object sender, RoutedEventArgs e)
        {
            NavigationContext.QueryString.TryGetValue("shoppingListId", out parameter);

            vm = new EditShoppingListVM();
            await vm.LoadProducts(Convert.ToInt32(parameter));
            DataContext = vm;
        }

        private void AddUsers_OnClick(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/ShoppingListUsersPage.xaml?shoppingListId=" + parameter, UriKind.Relative));
        }

        private async void ApplicationBarIconButton_OnClick(object sender, EventArgs e)
        {
            await vm.UpdateShoppingList();
        }

        private void ToggleButton_OnChecked(object sender, RoutedEventArgs e)
        {
            ((sender as CheckBox).DataContext as ShoppingListProduct).IsDeleted = true;
        }

        private async void DeleteButton_OnClick(object sender, EventArgs e)
        {
            if (DeleteBox.Visibility == Visibility.Collapsed)
            {
                DeleteBox.Visibility = Visibility.Visible;
                EnableButton(false);
            }
            else
            {
                await vm.RemoveProducts();
                DeleteBox.Visibility = Visibility.Collapsed;
                EnableButton(true);
            }
        }

        private void EnableButton(bool enable)
        {
            ApplicationBarIconButton b = (ApplicationBarIconButton)ApplicationBar.Buttons[0];
            b.IsEnabled = enable;
            ApplicationBarIconButton b2 = (ApplicationBarIconButton)ApplicationBar.Buttons[1];
            b2.IsEnabled = enable;
            ApplicationBarIconButton b3 = (ApplicationBarIconButton)ApplicationBar.Buttons[2];
            b3.IsEnabled = enable;
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            if (DeleteBox.Visibility == Visibility.Collapsed)
            {
                NavigationService.GoBack();
            }
            else
            {
                DeleteBox.Visibility = Visibility.Collapsed;
                EnableButton(true);
                e.Cancel = true;
            }
        }

        private void AddProduct_OnClick(object sender, EventArgs e)
        {
            vm.AddProduct("", Convert.ToInt32(parameter));
        }
    }
}