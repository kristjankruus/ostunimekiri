﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Ostunimekiri.Phone.ViewModels;

namespace Ostunimekiri.Phone
{
    public partial class RegisterPage : PhoneApplicationPage
    {
        public RegisterPage()
        {
            InitializeComponent();
        }

        private async void registerAccount_Click(object sender, RoutedEventArgs e)
        {
            RegisterVM vm = new RegisterVM();
            if (await vm.postAccount(userName.Text, password.Password, rePassword.Password) == true)
                NavigationService.GoBack();
        }
    }
}